﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication9.Entities
{
    class Actor
    {
        public int x, y;
        public string name;
        public int viata;

        public void die()
        {
            viata = 0;
            Console.WriteLine("Actoru {0} a murit", name);
        }

        public void move(int x1, int y1)
        {
            x = x1; y = y1;
            Console.WriteLine("Actoru {0} a mers la ({1},{2})", name, x, y);
        }
    }
}
