﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication9.Entities;

namespace WindowsFormsApplication9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //pictureBox1.BackgroundImage = Image.FromFile("c:\\temp\\vrajitor.jpg");

            Engineer E = new Engineer();
            E.name = "Dorel";
            Tank T = new Tank();
            T.name = " Panzer Dorel";
            E.move(10,10);
            T.move(10, 10);
            E.die();

            Plane P = new Plane();
            P.Fly();
        }
    }
}
